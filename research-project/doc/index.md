# Do Research Like a Munchkin - Team 1 - Research Project

This is the documentation of our findings from the research project carried out during the
[Do Research Like a Munchkin](https://the-munchkins.gitlab.io/do-research-like-a-munchkin) programming
workshop.

## The Brute-Force Method
While it certainly does not represent an ideal solution in most cases,
trying to solve a problem by brute force can often reveal some interesting
characteristics about the problem at hand.

Furthermore, if we remain in a realm where the brute-force method is computationally
feasible, it is always guaranteed to provide an exact solution and, with some
modifications, can also detect if there are multiple ways this solution can be
achieved.

In the case of our research project, the brute-force approach is relatively straightforward:
Go over all possible pairings of \(N\) particles (where \(N\) is even) and find the one
that minimizes the target function.

### Just how many possible combinations are there?
Let us consider an ordered list of points \( \{p_1, p_2, p_3, \dotsc, p_{N-1}, p_N\} \).
There are a total of \(N!\) ways in which we can arrange the particles in this ordered list.

If we now group particles into pairs going left to right, we get an ordered list of \(N/2\)
pairs of particles, e.g., \( \{(p_1, p_2), (p_3, p_4), \dotsc, (p_{N-1}, p_N)\} \), which
can be arranged in any of the \((N/2)!\) possible permutations.

Furthermore, any pair \((p_i, p_j)\) can be written in two ways; for \(N/2\) pairs,
this gives us a factor of \(2^{N/2}\).

The total number of combinations is therefore given by

\[
    \frac{N!}{(N/2)!2^{N/2}}.
\]

### How the algorithm works
The algorithm uses a simple recursive definition: Start by making a list of all
particle indices, and mark them all as available. Then, enter the recursive
part:

1.  If there are no more available indices, we have found a new solution:

    If the current \( \sum f \) is identical to the previous minimum, we have
    multiple equivalent solutions and append the new solution to the list of
    pairings. Otherwise, the solution is the new minimum.

2.  Otherwise, take the first available index as the first particle of the new pair
3.  Loop over all remaining available indices:

    Form the new pair and calculate the new target function: If it exceeds
    the current minimum, we do not need to consider this pair; continue
    with the next index from 3.

    Otherwise, add the new pair to the list of pairs, mark them as unavailable,
    and recursively call the function with the updated list of pairs, target
    function, and available indices.

4.  After all remaining indices have been checked, we will have the minimal
    possible solution for our given pairing.

### Efficiency of the algorithm
As the brute-force approach is guaranteed to find the exact minimum, performance
metrics will mainly be determined by its computational expense.

To reduce the amount of combinations which need to be examined, we employ two
additional techniques:

1.  The particles are sorted in decreasing order by the sum
    \( s_i = \sum\limits_{j\neq i} f(p_i, p_j). \) This way, we can make the
    sums of \( f \) grow quicker, which allows us to abort checking certain
    combinations much sooner.
2.  We use the result from the [Fast Pairing Method](#fast-pairing-method) as
    the initial minimum, again reducing the amount of combinations which need to
    be checked.

To investigate these modifications, we compare the "reference" time \( t_\text{ref} \),
i.e., the time taken by the algorithm from the previous section, and compare it to the
respective time \( t \) taken with our modifications. To account for fluctuations due
to spurious CPU loads, the times were all averaged over the same 100 iterations of
randomly generated points.

![Analysis of different brute-force approaches](./Analysis_BruteForce.png)

Evidently, the overhead resulting from sorting the particles and from evaluating
the guess results in higher computation times for a small number of particles,
but is quickly outweighed by the exponential scaling behavior of the method. The
dashed line in the figure above indicates the point where on average, the reference
and improved methods produce their solution in the same amount of time. In all tested
dimensions, only sorting the particles produces the least amount of overhead initially.
However, using the guess generated by the [Fast Pairing method](#fast-pairing-method)
together with sorting seems to provide the fastest solution for a sufficiently large
number of particles (about 12–16 depending on the number of dimensions).

Surprisingly, supplying only a good starting guess does not seem to
systematically improve the performance of the method (pay particular attention to
the 4-D graph, where the blue curve increases towards the end); however,
in combination with the sorting of particles, the cost is reduced
substantially at about 5–20% of the original cost for 20 particles,
depending on the number of dimensions.

The analysis was carried out using this [script](./Script_Analysis_BruteForce.py).

Another unexpected finding was the dependence of the computational time on the
number of dimensions. Given that the algorithm operates on a pre-computed
\( f \)-matrix, it was our expectation that the dimensionality of the problem
should have negligible effects on the computational expense, but the
opposite seems to be the case: As can be seen in the next figure, the average
times for the improved brute-force method increase drastically, indicating that
a far larger amount of possible combinations need to be examined.

![Effect of the dimensionality on the brute-force approach](./Analysis_BruteForce_Dimensionality.png)

The analysis was carried out using this [script](./Script_Analysis_BruteForce_Dimensionality.py).

In summary, the findings using this algorithm are:

*   By not investigating "hopeless" combinations any further than needed, using
    a decent starting guess supplied by a cheaper method, and making the sum
    grow as quickly as possible, the brute-force approach can be substantially
    improved compared to a primitive implementation which simply checks all
    possible combinations.
*   The number of dimensions has a far larger effect on the performance than
    we initially expected.
*   Despite the above mentioned improvements, the algorithm becomes infeasibly
    expensive very quickly, so we recommend not using it for more than 20–30
    particles, depending on the number of dimensions.

## Fast Pairing Method
This is a simple algorithm that tries to find the particle
that has the highest value of the objective function \( f \) when paired with any other
and then pairs it with the particle which produces the smallest value of \( f \) when paired with it.
This algorithm will always find a relatively low sum of \( f \) of pairs, however, it does not have to be the optimal pairing.
In the case when a distance measure is used for the function \( f \) and 1-D points this method yields rigorously the best pairing.
### How the algorithm works
The central quantity is the \( f \)-of-pairs-matrix which contains the value of \( f \) for all possible pairs.
The diagonal elements are masked, as they do not represent any real pairing.
Then the sum along each row of the matrix is computed. The algorithm works then as follows:

1. Find the row with the highest sum.
2. In that row find the lowest element.
3. Pair the two particles with one another.
4. Mask all rows and columns in which the two particles appear.
5. Start again with 1 until all particles have been paired.


### Efficiency of the algorithm
![Placeholder](./RandomPairs_vs_Fast.png)
The quality of the pairing found by this algorithm is determined by computing its relative deviation from the optimal result
which in turn is compared to the relative deviation of a random pairing from the optimal pairing.
The relative deviation is computed for 2 to 20 points in 1-D to 4-D.
The values are reported as means over \( 10^4 \) arrangements of points.

One can clearly see that random pairings become "better" with increasing dimensionality.
The authors propose that this has to do with the combinations simply being less different
due to increased flexibility in higher dimensions.
However, the relative error always increases monotonously with the number of points.
In contrast, the fast algorithm is, as stated before, exact in 1-D and
its relative error seems to level off around 10% for many points.
It is hard to draw definite conclusions from the above plot for more than 20 points.
Unfortunately the exact brute-force algorithm becomes exceedingly expensive
such that averages over many runs become very expensive.

The analysis was carried out with this [script](./Script_Analysis_Fast.py).

## Monte Carlo Approach

Monte Carlo methods are named after the famous casinos as they are built around using random numbers.
We have implemented a Metropolis Monte Carlo method that tries to improve a given pairing by performing random exchanges.
Such a potential swap of indices is accepted according to the Metropolis criterion, i.e.,
the change is always accepted if it constitutes an improvement
or no change regarding the objective function (in our case the sum over \( f(\text{pairs}) \)).
If the potential swap increases the sum, it is only accepted with the probability \(  e^{- \alpha\cdot\Delta} \),
where \( \alpha \) is a scale factor and \( \Delta \) is the change in the objective function.
In canonical molecular simulations, \( \alpha \) is the inverse thermal energy and \( \Delta \) the change in energy.

### How our algorithm works
1. Select randomly two pairs.
2. Select randomly one point/particle/index in each pair.
3. Evaluate \( \sum f \) for the new pairing.
4. Accept the new pairing according to the Metropolis criterion.
5. Keep track of the lowest possible pairing ever visited.
6. Stop the loop after a predetermined number of attempted swaps.

### Efficiency of the algorithm
The MC approach has several parameters which can be tweaked in order to improve its success rate.
In general, there are three important settings:

1. The pairing passed to the algorithm.
   This will start the algorithm at position where the value of \( \sum f \) is closer to the desired value,
   but it might be further removed in pairing space.
2. The "temperature" at which the simulation is carried out.
   A higher "temperature" (lower \( \alpha \)) which scales \( \Delta \) down more strongly leads to a higher acceptance rate.
   This will enable easy escape of local minima,
   but can also carry the simulation away from the desired global minimum.
3. The number of MC steps performed before the algorithm stops.
   A higher number of steps will always increase the chance of finding the global minimum,
   but makes the method more costly.

![Placeholder](./Analysis_MC.png)
In order to analyse the algorithms the relative divergence from the optimal result was computed
and averaged over \( 10^4 \) calculations.
From the analysis presented in the figure, one can draw the following conclusions:

1. A better pairing will start the algorithm at a much lower error, however,
   the [fast pairing](#fast-pairing-method) is already so good that the MC method
   can only improve it so much. Especially for many points the improvement seems
   to be highly coupled to the scaling factor.
2. A high "temperature" does not improve convergence, more the opposite.
   It seems to carry the system away from the desired minimum.
   It is assumed that a temperature of \( T=10 \) nearly accepts all moves,
   and therefore there is little difference to even higher temperatures.
   Lower "temperatures" seem to enhance convergence up to a point where the scaling becomes so strong that the
   simulation becomes trapped in a minimum from which it cannot escape.
3. As assumed, an increasing number of steps improves the pairing.
   However, the number of random swaps needed to find the optimum increases quickly with the number of points.
   Already for 16 points \( 10^4 \) swaps starting from a random pairing
   do not reach on average the quality of the [fast pairing](#fast-pairing-method),
   which is found extremely quickly.


The analysis was carried out with this [script](./Script_Analysis_MC.py).


## K-Means Clustering

The K-Means algorithm can be used to cluster points based on their euclidean distance.
The aim of the algorithm is to choose centroids that minimize the inertia, or within cluster sum of squares criterion:

$$\sum_{i=0}^n \min_{\mu_j \in C} (||x_j - \mu_i||^2)$$

This can be used to sort a large number N of points into k smaller clusters, which are easier to handle for
the previously mentioned pair-finding algorithms.

### How the algorithm works
* In the first step initial centroids are chosen. The convergence of the algorithm is highly dependent on this choice.
It is therefore common to run the simulation several times, with different initializations of the centroids.
In order to improve the quality of initial guesses, the 'k-means++' algorithm can be used. This ensures the centroids to
  be distant to each other, leading to better results than purely random initialization.

* The algorithm now loops self-consistently between the following steps, until convergence is reached:
    * assign each sample to its nearest centroid.
    * generate new centroids by taking the mean value of all the samples assigned to each previous centroid.
    * compute the difference between the old and new centroids.

### KMeans and Solve
The *KMeans and Solve* algorithm is a combination of the bruteforce approach and KMeans clustering.
The Idea of the algorithm is to sort the points into N clusters and evaluate the exact pairing in each one of them.
This involves the following three steps:
* Cluster the points into N clusters
* if there are clusters with an uneven number of elements remove the points with the shortest distance to another clusters of uneven size
* loop over all clusters and use the bruteforce algorithm to pair their points

### Efficiency of the Algorithm
The quality of the pairing found by this algorithm is determined by computing its relative deviation from the optimal result.
The relative deviation is computed for 4 to 20 points in 1-D to 4-D.
The values are reported as means over 500 random arrangements of points.
![Placeholder](./KMeans_errors.png)
From the analysis presented in the figure, one can draw the following conclusions:
* it always returns the exact result for 1D and 2 clusters.
* because errors are only introduced by the clustering the error grows with the number of clusters.
* errors will therefore always approach a unique limit for each number of clusters.
* taking aside the 1D special case the dimensionality does not affect the accuracy.

The analysis was carried out with this [script](./Script_Analysis_KMeans.py).

### Resources
All clustering algorithms that you can use are based on the K-Means implementation provided by
the [scikit-learn](https://sklearn.org/modules/clustering.html) python module.


## Comparison of the Different Methods for 100 Particles
With these methods in hand, we now face the actual challenge of the workshop: producing
a solution for 100 \( N \)-dimensional particles in less than 30 seconds that is as close
to the global minimum as possible.

For obvious reasons, the [brute-force method](#the-brute-force-method) is not an option
for such a large amount of particles due to its unfavorable scaling behavior, so the
comparisons will be between the [Fast Pairing method](#fast-pairing-method), the
[Monte Carlo method](#monte-carlo-approach) and the [K-Means method](#k-means-clustering).

For our benchmarking calculations, we recorded the computational times and the target
functions of the different methods from 1-D to 6-D, averaged over 100 iterations with
different seeds for the random number generator. [Monte Carlo](#monte-carlo-approach)
simulations were carried out for \( 10^4 \) iterations with a [Fast Pairing](#fast-pairing-method)
guess and a temperature of \( T=0.1 \). [K-Means clustering](#k-means-clustering) was
carried out with a minimum of eight clusters (the number of clusters can be dynamically
increased to ensure all clusters are even-sized).

![100-Particles](./Analysis_100_Particles.png)

As can be seen in the figure above, both the [K-Means](#k-means-clustering) and
[Monte Carlo](#monte-carlo-approach) approaches hardly offer any tangible improvement
over the [Fast Pairing method](#fast-pairing-method), but bring with them significantly
increased computation times. While the computational overhead in the
[Monte Carlo method](#monte-carlo-approach) is determined by the supplied number of
iterations to carry out and is therefore more or less constant, the
[K-Means approach](#k-means-clustering) internally solves the pairing problem for
its clusters by [brute force](#the-brute-force-method), which, as we established
above, becomes more and more expensive as the number of dimensions increases.
To cope with the increased computational cost, the number of clusters would need to
be adjusted upwards, leading to larger errors in the solution and ultimately providing
worse results than the much quicker [Fast Pairing method](#fast-pairing-method).

These findings corroborate our assumption that the [Fast Pairing method](#fast-pairing-method)
produces very respectable results even for large numbers of particles and dimensions.
While the [K-Means](#k-means-clustering) and [Monte Carlo](#monte-carlo-approach) methods
can provide improvements over the [Fast Pairing](#fast-pairing-method) guess, they are
in no way guaranteed to do so. Combined with the higher computational cost, their usage
respresents a tradeoff between potentially better results and increased runtimes.

The analysis was carried out with this [script](./Script_Analysis_100_Particles.py).

