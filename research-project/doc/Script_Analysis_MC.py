# This script was executed with the version of the package
# commit 23f5b6b0eb7084e0fadc59ed30976265a5d1b98b

import numpy as np
import matplotlib.pyplot as plt

from particle_generator import ParticleGenerator
from find_minimal_pairs import BestPairsFinder

data = []


n_dim = 2
n_points = 10
pairing = [(2*i, 2*i+1) for i in range(n_points//2)]
d10 = []
d16 = []
for ii, n_points in enumerate([10, 16]):
    for T in [0.01, 0.1, 1.0, 10.0, 100.0]:
        temp_list = []
        for n_moves in [10, 50, 100, 500, 1000, 5000, 10000]:
            test_list = []
            for _ in range(1000):
                test_points = ParticleGenerator.generate_particles(n_points, n_dim)
                PairFinder.points = test_points
                fast_result = PairFinder.find_pairs()
                MC_result_f = deepcopy(PairFinder.pairs_MC(fast_result[0], T=T, n_moves=n_moves))
                MC_result_r = deepcopy(PairFinder.pairs_MC(pairing, T=T, n_moves=n_moves))
                test_list.append([fast_result[1], MC_result_r[1], MC_result_f[1], PairFinder.find_pairs_recursive(lowest_guess=fast_result[1])[1]])
    
            test_list = np.asarray(test_list)
            temp_list.append([n_points, ((test_list[:,0]-test_list[:,3])/test_list[:,3]).mean(),
                                            ((test_list[:,1]-test_list[:,3])/test_list[:,3]).mean(),
                                            ((test_list[:,2]-test_list[:,3])/test_list[:,3]).mean()])
        if ii == 0:
            d10.append(temp_list)
        if ii == 1:
            d16.append(temp_list)


d10 = np.array(d10)
d16 = np.array(d16)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams["figure.figsize"] = [12,10]
fig, axs = plt.subplots(2, 2, sharex=True, sharey=False)

axs[0,0].plot(n_moves, d10[0][:,2], linewidth=3, linestyle="-", color="darkorange", label=r"$T$=0.01")
axs[0,0].plot(n_moves, d10[1][:,2], linewidth=3, linestyle="-", color="darkblue", label=r"$T$=0.1")
axs[0,0].plot(n_moves, d10[2][:,2], linewidth=3, linestyle="-", color="pink", label=r"$T$=1")
axs[0,0].plot(n_moves, d10[3][:,2], linewidth=3, linestyle="-", color="darkgreen", label=r"$T$=10")
axs[0,0].plot(n_moves, d10[4][:,2], linewidth=3, linestyle="-", color="red", label=r"$T$=100")
axs[0,1].plot(n_moves, d10[0][:,3], linewidth=3, linestyle="-", color="darkorange", label=r"$T$=0.01")
axs[0,1].plot(n_moves, d10[1][:,3], linewidth=3, linestyle='-', color="darkblue", label=r"$T$=0.1")
axs[0,1].plot(n_moves, d10[2][:,3], linewidth=3, linestyle='-', color="pink", label=r"$T$=1")
axs[0,1].plot(n_moves, d10[3][:,3], linewidth=3, linestyle='-', color="darkgreen", label=r"$T$=10")
axs[0,1].plot(n_moves, d10[4][:,3], linewidth=3, linestyle='-', color="red", label=r"$T$=100")

axs[1,0].plot(n_moves, d16[0][:,2], linewidth=3, linestyle="-", color="darkorange", label=r"$T$=0.01")
axs[1,0].plot(n_moves, d16[1][:,2], linewidth=3, linestyle="-", color="darkblue", label=r"$T$=0.1")
axs[1,0].plot(n_moves, d16[2][:,2], linewidth=3, linestyle="-", color="pink", label=r"$T$=1")
axs[1,0].plot(n_moves, d16[3][:,2], linewidth=3, linestyle="-", color="darkgreen", label=r"$T$=10")
axs[1,0].plot(n_moves, d16[4][:,2], linewidth=3, linestyle="-", color="red", label=r"$T$=100")
axs[1,1].plot(n_moves, d16[0][:,3], linewidth=3, linestyle="-", color="darkorange", label=r"$T$=0.01")
axs[1,1].plot(n_moves, d16[1][:,3], linewidth=3, linestyle='-', color="darkblue", label=r"$T$=0.1")
axs[1,1].plot(n_moves, d16[2][:,3], linewidth=3, linestyle='-', color="pink", label=r"$T$=1")
axs[1,1].plot(n_moves, d16[3][:,3], linewidth=3, linestyle='-', color="darkgreen", label=r"$T$=10")
axs[1,1].plot(n_moves, d16[4][:,3], linewidth=3, linestyle='-', color="red", label=r"$T$=100")


axs[0,0].set_title(r"Random Pairing, $N_\mathrm{Points}$=10", fontsize=25)
axs[0,1].set_title(r"Fast Pairing, $N_\mathrm{Points}$=10", fontsize=25)
axs[1,0].set_title(r"Random Pairing, $N_\mathrm{Points}$=16", fontsize=25)
axs[1,1].set_title(r"Fast Pairing, $N_\mathrm{Points}$=16", fontsize=25)

for ax in axs.flatten():

    ax.spines['bottom'].set_linewidth('3')
    ax.spines['top'].set_linewidth('3')
    ax.spines['left'].set_linewidth('3')
    ax.spines['right'].set_linewidth('3')
    ax.tick_params(axis='y',length=6,width=3,labelsize=20, pad=10, direction='in')
    ax.tick_params(axis='x',length=6,width=3,labelsize=20, pad=10, direction='in')
    ax.legend(loc='best', fontsize=20, frameon=False)
    ax.set_xscale('log')

axs[0,0].set_xlim([5, 2e4])
axs[1,0].set_xlabel(r'$N_\mathrm{moves}$', fontsize=25) 
axs[1,1].set_xlabel(r'$N_\mathrm{moves}$', fontsize=25) 
axs[0,0].set_ylabel(r'$\left<\frac{(\sum f)_\mathrm{approx} - (\sum f)_\mathrm{opt}}{(\sum f)_\mathrm{opt}}\right>$', fontsize=35)
axs[1,0].set_ylabel(r'$\left<\frac{(\sum f)_\mathrm{approx} - (\sum f)_\mathrm{opt}}{(\sum f)_\mathrm{opt}}\right>$', fontsize=35)

plt.tight_layout()
plt.savefig("Analysis_MC.png", dpi=400)
plt.close()
