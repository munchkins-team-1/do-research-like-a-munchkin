"""Script to analyze the performance of the brute-force method
with various optimizations.

This script was run with commit #1f086e451f2891ed0bcb5b981f674e55905e572a

To run the script, get hold of the source code, "build" it by running

```
cp -r src find_minimal_pairs
```

and execute the script using

```
PYTHONPATH=<directory containing find_minimal_pairs> python3 Script_Analysis_BruteForce.py
```
"""


import time
import numpy as np
from matplotlib import pyplot as plt
from find_minimal_pairs import (BestPairsFinder,
                                BruteForceFinder,
                                FastPairingFinder,
                                ParticleGenerator)


# Time the execution of a function
def time_solution(finder: BestPairsFinder) -> float:
    start = time.perf_counter()
    finder.solve()
    end = time.perf_counter()

    return end - start


points_limits = (6, 21) # upper limit exclusive


# Setup variables
n_iter = 100
dimensions = list(range(1, 5))
numbers_of_points = list(range(*points_limits, 2))


timings = np.zeros((len(dimensions), len(numbers_of_points), 3))


finder_ref        = BruteForceFinder(BestPairsFinder.distance,
                                     [],
                                     pre_sort=False,
                                     starting_guess=None)
finder_guess_only = BruteForceFinder(BestPairsFinder.distance,
                                     [],
                                     pre_sort=False,
                                     starting_guess=FastPairingFinder)
finder_sort_only  = BruteForceFinder(BestPairsFinder.distance,
                                     [],
                                     pre_sort=True,
                                     starting_guess=None)
finder_both       = BruteForceFinder(BestPairsFinder.distance,
                                     [],
                                     pre_sort=True,
                                     starting_guess=FastPairingFinder)


# Benchmark...
for i, n_dim in enumerate(dimensions):
    for j, n_points in enumerate(numbers_of_points):
        print(f"Ndim = {n_dim:2d}, Npoints = {n_points:2d} ", end="", flush=True)

        t_ref        = np.zeros(n_iter)
        t_guess_only = np.zeros(n_iter)
        t_sort_only  = np.zeros(n_iter)
        t_both       = np.zeros(n_iter)

        for k in range(n_iter):
            points = ParticleGenerator.generate_particles(n_points, n_dim,
                                                          seed=(i * len(numbers_of_points) * n_iter + j * n_iter + k))

            finder_ref.points        = points
            finder_guess_only.points = points
            finder_sort_only.points  = points
            finder_both.points       = points

            t_ref[k]        = time_solution(finder_ref)
            t_guess_only[k] = time_solution(finder_guess_only)
            t_sort_only[k]  = time_solution(finder_sort_only)
            t_both[k]       = time_solution(finder_both)

            if n_iter < 10 or k % (n_iter // 10) == 0:
                print(".", end="", flush=True)

        timings[i, j, 0] = np.mean(t_guess_only / t_ref)
        timings[i, j, 1] = np.mean(t_sort_only / t_ref)
        timings[i, j, 2] = np.mean(t_both / t_ref)
        print()


# ... and prepare plot
plt.rcParams.update({"text.usetex": True,
                     "font.family": "serif",
                     "figure.figsize": (12, 10)})

fig, axes = plt.subplots(nrows=2, ncols=2, sharex=True, sharey=False)

for i in range(2):
    for j in range(2):
        n_dim = 2 * i + j

        axes[i, j].set_title(f"{n_dim + 1:d}-D", fontsize=25)

        axes[i, j].set_xlim(points_limits)

        axes[i, j].plot(points_limits, [1, 1], "k--")

        axes[i, j].plot(numbers_of_points, timings[n_dim, :, 0], label="guess only")
        axes[i, j].plot(numbers_of_points, timings[n_dim, :, 1], label="sort only")
        axes[i, j].plot(numbers_of_points, timings[n_dim, :, 2], label="guess + sort")


plt.setp(axes, xticks=list(range(*points_limits, 4)))

for ax in axes.flatten():
    ax.spines["bottom"].set_linewidth("3")
    ax.spines["top"].set_linewidth("3")
    ax.spines["left"].set_linewidth("3")
    ax.spines["right"].set_linewidth("3")
    ax.tick_params(axis="x", length=6, width=3, labelsize=20, pad=10, direction="in")
    ax.tick_params(axis="y", length=6, width=3, labelsize=20, pad=10, direction="in")
    ax.legend(loc="best", fontsize=16, frameon=False)

axes[1, 0].set_xlabel(r"$N_\mathrm{Points}$", fontsize=25)
axes[1, 1].set_xlabel(r"$N_\mathrm{Points}$", fontsize=25)
axes[0, 0].set_ylabel(r"$\left< \frac{t}{t_\mathrm{ref}} \right>$", fontsize=25)
axes[1, 0].set_ylabel(r"$\left< \frac{t}{t_\mathrm{ref}} \right>$", fontsize=25)


plt.tight_layout()
# plt.show()
plt.savefig("Analysis_BruteForce.png", dpi=400)
plt.close()

