# This script was executed with the version of the package
# commit 1adfbb4c01c545efd4b8dfd99dc94810d07821bd

import numpy as np
import matplotlib.pyplot as plt
import time

from src import BestPairsFinder
from src import ParticleGenerator
from src import FastPairingFinder
from src import BruteForceFinder
from src import KMeansFinder

def analyse_kmeans(n_samples=1):
    data = []
    for ii, n_dim in enumerate([1, 2, 3, 4]):
        print("Analyse {dim}D ...".format(dim=n_dim))
        for n_points in [4, 8, 12, 16, 20]:

            test_list = np.array([0.0, 0.0, 0.0, 0.0])
            time_fast = 0
            time_kmeans = np.array([0.0, 0.0])

            for _ in range(n_samples):
                test_points = ParticleGenerator.generate_particles(n_points, n_dim)

                brute = BruteForceFinder(BestPairsFinder.distance, test_points, starting_guess=FastPairingFinder)
                brute_result = brute.solve()

                start = time.perf_counter()
                fast = FastPairingFinder(BestPairsFinder.distance, test_points)
                fast_result = fast.solve()
                time_fast += time.perf_counter() - start

                test_list = np.vstack((test_list, [brute_result[1], fast_result[1], 0.0, 0.0]))

                for n, nclusters in enumerate([2, 4]):
                    start = time.perf_counter()
                    kmeans = KMeansFinder(BestPairsFinder.distance, test_points, n_clusters=nclusters)
                    kmeans_result = kmeans.solve()
                    time_kmeans[n] += time.perf_counter() - start
                    test_list[-1, n+2] = kmeans_result[1]

            data.append([n_points, n_dim, time_fast/n_samples, time_kmeans[0]/n_samples, time_kmeans[1]/n_samples,
                         np.abs(((test_list[1:,1]-test_list[1:,0])/test_list[1:,0]).mean()),
                         np.abs(((test_list[1:,2]-test_list[1:,0])/test_list[1:,0]).mean()),
                         np.abs(((test_list[1:,3]-test_list[1:,0])/test_list[1:,0]).mean())])

    return np.asarray(data)


if __name__ == "__main__":

    data = analyse_kmeans(500)

    one_dim = data[data[:,1] == 1]
    two_dim = data[data[:,1] == 2]
    three_dim = data[data[:,1] == 3]
    four_dim = data[data[:,1] == 4]
    
    # plot timing
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    plt.rcParams["figure.figsize"] = [15, 5]
    fig, axs = plt.subplots(1, 2, sharex=True)
    
    axs[0].plot(one_dim[:, 0], one_dim[:, 3], linewidth=3, linestyle="-", color="darkorange", label="1-D")
    axs[0].plot(one_dim[:, 0], two_dim[:, 3], linewidth=3, linestyle='-', color="darkblue", label="2-D")
    axs[0].plot(one_dim[:, 0], three_dim[:, 3], linewidth=3, linestyle='-', color="red", label="3-D")
    axs[0].plot(one_dim[:, 0], four_dim[:, 3], linewidth=3, linestyle='-', color="darkgreen", label="4-D")
    
    axs[1].plot(one_dim[:, 0], one_dim[:, 4], linewidth=3, linestyle="-", color="darkorange", label="1-D")
    axs[1].plot(one_dim[:, 0], two_dim[:, 4], linewidth=3, linestyle='-', color="darkblue", label="2-D")
    axs[1].plot(one_dim[:, 0], three_dim[:, 4], linewidth=3, linestyle='-', color="red", label="3-D")
    axs[1].plot(one_dim[:, 0], four_dim[:, 4], linewidth=3, linestyle='-', color="darkgreen", label="4-D")
    
    axs[0].set_title("KMeans and Solve, 2 Clusters", fontsize=25)
    axs[1].set_title("KMeans and Solve, 3 Clusters", fontsize=25)
    
    for ax in axs:
        ax.set_xlabel(r'$N_\mathrm{Points}$', fontsize=25)
        ax.spines['bottom'].set_linewidth('3')
        ax.spines['top'].set_linewidth('3')
        ax.spines['left'].set_linewidth('3')
        ax.spines['right'].set_linewidth('3')
        ax.grid()
        ax.tick_params(axis='y', length=6, width=3, labelsize=20, pad=10, direction='in')
        ax.tick_params(axis='x', length=6, width=3, labelsize=20, pad=10, direction='in')
        ax.legend(loc='best', fontsize=20, frameon=False)
    
    axs[0].set_ylabel(r'$\left<t\right>$ (s)', fontsize=35)
    
    plt.tight_layout()
    plt.savefig("KMeans_timing.png", dpi=400)
    plt.close()
    
    # plot error
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    plt.rcParams["figure.figsize"] = [15, 5]
    fig, axs = plt.subplots(1, 2, sharex=True)
    
    axs[0].plot(one_dim[:, 0], one_dim[:, 6], linewidth=3, linestyle="-", color="darkorange", label="1-D")
    axs[0].plot(one_dim[:, 0], two_dim[:, 6], linewidth=3, linestyle='-', color="darkblue", label="2-D")
    axs[0].plot(one_dim[:, 0], three_dim[:, 6], linewidth=3, linestyle='-', color="red", label="3-D")
    axs[0].plot(one_dim[:, 0], four_dim[:, 6], linewidth=3, linestyle='-', color="darkgreen", label="4-D")
    
    axs[1].plot(one_dim[:, 0], one_dim[:, 7], linewidth=3, linestyle="-", color="darkorange", label="1-D")
    axs[1].plot(one_dim[:, 0], two_dim[:, 7], linewidth=3, linestyle='-', color="darkblue", label="2-D")
    axs[1].plot(one_dim[:, 0], three_dim[:, 7], linewidth=3, linestyle='-', color="red", label="3-D")
    axs[1].plot(one_dim[:, 0], four_dim[:, 7], linewidth=3, linestyle='-', color="darkgreen", label="4-D")
    
    axs[0].set_title("KMeans and Solve, 2 Clusters", fontsize=25)
    axs[1].set_title("KMeans and Solve, 4 Clusters", fontsize=25)
    
    for ax in axs:
        ax.set_xlabel(r'$N_\mathrm{Points}$', fontsize=25)
        ax.spines['bottom'].set_linewidth('3')
        ax.spines['top'].set_linewidth('3')
        ax.spines['left'].set_linewidth('3')
        ax.spines['right'].set_linewidth('3')
        ax.grid()
        ax.tick_params(axis='y', length=6, width=3, labelsize=20, pad=10, direction='in')
        ax.tick_params(axis='x', length=6, width=3, labelsize=20, pad=10, direction='in')
        ax.legend(loc='best', fontsize=20, frameon=False)
    
    axs[0].set_ylabel(r'$\left<\frac{(\sum f)_\mathrm{approx} - (\sum f)_\mathrm{opt}}{(\sum f)_\mathrm{opt}}\right>$',
                      fontsize=35)
    
    plt.tight_layout()
    plt.savefig("KMeans_errors.png", dpi=400)
    plt.close()
