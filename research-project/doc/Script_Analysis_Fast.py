# This script was executed with the version of the package
# commit 23f5b6b0eb7084e0fadc59ed30976265a5d1b98b

import numpy as np
import matplotlib.pyplot as plt

from particle_generator import ParticleGenerator
from find_minimal_pairs import BestPairsFinder

data = []

for ii, n_dim in enumerate([1, 2, 3, 4]):
    dim_list = []
    data.append([])
    for n_points in [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]:
        pairing = [(2*i, 2*i+1) for i in range(n_points//2)]
        test_list = []
        for _ in range(10000):
            test_points = ParticleGenerator.generate_particles(n_points, n_dim)
            PairFinder.points = test_points
            fast_result = PairFinder.find_pairs()
            brute_force = PairFinder.find_pairs_recursive(lowest_guess = fast_result[1])
            random_result = PairFinder.evaluate_pairing(pairing)
            test_list.append([random_result, fast_result[1], brute_force[1]])

        test_list = np.asarray(test_list)
        dim_list.append([n_points, ((test_list[:,0]-test_list[:,2])/test_list[:,2]).mean(),
                                            ((test_list[:,1]-test_list[:,2])/test_list[:,2]).mean()])
        #
    data[ii].append(dim_list)

data = np.array(data)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams["figure.figsize"] = [15,8]
fig, axs = plt.subplots(1, 2, sharex=True)

axs[0].plot(data[0][:,0], data[0][:, 1], linewidth=3, linestyle="-", color="darkorange", label="1-D")
axs[1].plot(data[0][:,0], data[0][:, 2], linewidth=3, linestyle='-', color="darkorange", label="1-D")
axs[0].plot(data[1][:,0], data[1][:, 1], linewidth=3, linestyle="-", color="darkblue", label="2-D")
axs[1].plot(data[1][:,0], data[1][:, 2], linewidth=3, linestyle='-', color="darkblue", label="2-D")
axs[0].plot(data[2][:,0], data[2][:, 1], linewidth=3, linestyle="-", color="red", label="3-D")
axs[1].plot(data[2][:,0], data[2][:, 2], linewidth=3, linestyle='-', color="red", label="3-D")
axs[0].plot(data[3][:,0], data[3][:, 1], linewidth=3, linestyle="-", color="darkgreen", label="4-D")
axs[1].plot(data[3][:,0], data[3][:, 2], linewidth=3, linestyle='-', color="darkgreen", label="4-D")

axs[0].set_title("Random Pairing", fontsize=25)
axs[1].set_title("Fast Pairing", fontsize=25)

for ax in axs:
    ax.set_xlabel(r'$N_\mathrm{Points}$', fontsize=25) 
    ax.spines['bottom'].set_linewidth('3')
    ax.spines['top'].set_linewidth('3')
    ax.spines['left'].set_linewidth('3')
    ax.spines['right'].set_linewidth('3')
    ax.tick_params(axis='y',length=6,width=3,labelsize=20, pad=10, direction='in')
    ax.tick_params(axis='x',length=6,width=3,labelsize=20, pad=10, direction='in')
    ax.legend(loc='best', fontsize=20, frameon=False)

axs[0].set_ylabel(r'$\left<\frac{(\sum f)_\mathrm{approx} - (\sum f)_\mathrm{opt}}{(\sum f)_\mathrm{opt}}\right>$', fontsize=35)
  
plt.tight_layout()
plt.savefig("RandomPairs_vs_Fast.png", dpi=400)
plt.close()
