"""Script to analyze the performance of various methods for
100 n-dimensional particles as posed in the original challenge.

This script was run with commit #1adfbb4c01c545efd4b8dfd99dc94810d07821bd

To run the script, get hold of the source code, "build" it by running

```
cp -r src find_minimal_pairs
```

and execute the script using

```
PYTHONPATH=<directory containing find_minimal_pairs> python3 Script_Analysis_100_Particles.py
```
"""


import time
from dataclasses import dataclass
from typing import Dict, List, Tuple
import numpy as np
from matplotlib import pyplot as plt
from find_minimal_pairs import (BestPairsFinder,
                                FastPairingFinder,
                                KMeansFinder,
                                MonteCarloFinder,
                                ParticleGenerator,
                                )


@dataclass
class Result:
    pairing: List[Tuple[int, int]]
    target_function: float
    elapsed_time: float


def solve_100_particles_in_n_dim(n_dim: int,
                                 seed: int,
                                 kmeans_clusters: int,
                                 mc_moves: int,
                                 mc_temp: float) -> Dict[str, Result]:
    particles = ParticleGenerator.generate_particles(100, n_dim, seed=seed)

    fast_finder = FastPairingFinder(BestPairsFinder.distance, particles)
    kmeans_finder = KMeansFinder(BestPairsFinder.distance, particles, kmeans_clusters)
    monte_carlo_finder = MonteCarloFinder(BestPairsFinder.distance, particles, mc_moves, mc_temp)

    return {"fast": time_solution(fast_finder),
            "kmeans": time_solution(kmeans_finder),
            "mc": time_solution(monte_carlo_finder),
            }


def time_solution(finder: BestPairsFinder) -> Result:
    start = time.perf_counter()
    pairing, target_function = finder.solve()
    end = time.perf_counter()

    return Result(pairing=pairing,
                  target_function=target_function,
                  elapsed_time=(end - start))


def main() -> None:
    kmeans_clusters = 8
    mc_moves = 10000
    mc_temp = 0.1

    dimensions = np.arange(1, 7)

    totals = [{method: Result([], 0.0, 0.0) for method in ("fast", "kmeans", "mc")} for _ in range(len(dimensions))]

    n_runs = 100
    for i in range(n_runs):
        print(f"Run {i+1} of {n_runs}:", end="", flush=True)
        for n_dim in dimensions:
            result = solve_100_particles_in_n_dim(n_dim,
                                                  seed=(n_runs * n_dim + i),
                                                  kmeans_clusters=kmeans_clusters,
                                                  mc_moves=mc_moves,
                                                  mc_temp=mc_temp)
            print(f" {n_dim}", end="", flush=True)

            totals[n_dim - 1]["fast"].target_function += result["fast"].target_function
            totals[n_dim - 1]["fast"].elapsed_time += result["fast"].elapsed_time

            totals[n_dim - 1]["kmeans"].target_function += result["kmeans"].target_function
            totals[n_dim - 1]["kmeans"].elapsed_time += result["kmeans"].elapsed_time

            totals[n_dim - 1]["mc"].target_function += result["mc"].target_function
            totals[n_dim - 1]["mc"].elapsed_time += result["mc"].elapsed_time
        print("", flush=True)


    fast_data = np.array([[result["fast"].target_function, result["fast"].elapsed_time] for result in totals]) / n_runs
    kmeans_data = np.array([[result["kmeans"].target_function, result["kmeans"].elapsed_time] for result in totals]) / n_runs
    mc_data = np.array([[result["mc"].target_function, result["mc"].elapsed_time] for result in totals]) / n_runs

    bar_width = 0.25

    plt.rc("text", usetex=True)
    plt.rc("font", family="serif")
    plt.rcParams["figure.figsize"] = 12, 5

    fig, axes = plt.subplots(nrows=1, ncols=2)

    for ax in axes:
        ax.spines["bottom"].set_linewidth("3")
        ax.spines["top"].set_linewidth("3")
        ax.spines["left"].set_linewidth("3")
        ax.spines["right"].set_linewidth("3")
        ax.tick_params(axis="x", length=6, width=3, labelsize=20, pad=10, direction="in")
        ax.tick_params(axis="y", length=6, width=3, labelsize=20, pad=10, direction="in")
        ax.legend(loc="best", fontsize=16, frameon=False)

    axes[0].set_xlabel(r"$N_\mathrm{dim}$", fontsize=25)
    axes[0].set_ylabel(r"$\left<{\left(\sum f\right)}_\mathrm{method}\right> / \left<{\left(\sum f\right)}_\mathrm{Fast}\right>$", fontsize=25)

    axes[1].set_xlabel(r"$N_\mathrm{dim}$", fontsize=25)
    axes[1].set_ylabel(r"$\left<t_\mathrm{method}\right>$ / s", fontsize=25)

    kmeans_sums = kmeans_data[:, 0] / fast_data[:, 0]
    mc_sums = mc_data[:, 0] / fast_data[:, 0]
    axes[0].bar(dimensions - bar_width, 1.0,         width=bar_width, color="red", label="Fast Pairing")
    axes[0].bar(dimensions,             kmeans_sums, width=bar_width, color="darkblue", label=r"K-Means ($\geq 8$ clusters)")
    axes[0].bar(dimensions + bar_width, mc_sums,     width=bar_width, color="darkgreen", label=r"Monte Carlo ($10^4$ moves, $T=0.1$)")
    axes[0].legend()

    fast_times = fast_data[:, 1]
    kmeans_times = kmeans_data[:, 1]
    mc_times = mc_data[:, 1]
    axes[1].bar(dimensions - bar_width, fast_times,   width=bar_width, color="red", label="Fast Pairing")
    axes[1].bar(dimensions,             kmeans_times, width=bar_width, color="darkblue", label=r"K-Means ($\geq 8$ clusters)")
    axes[1].bar(dimensions + bar_width, mc_times,     width=bar_width, color="darkgreen", label=r"Monte Carlo ($10^4$ moves, $T=0.1$)")
    axes[1].legend()

    plt.setp(axes, xticks=dimensions)
    plt.tight_layout()
    # plt.show()
    plt.savefig("Analysis_100_Particles.png", dpi=400)
    plt.close()


if __name__ == "__main__":
    main()

