"""Script to analyze the performance of the brute-force method
depending on the number of dimensions.

This script was run with commit #1f086e451f2891ed0bcb5b981f674e55905e572a

To run the script, get hold of the source code, "build" it by running

```
cp -r src find_minimal_pairs
```

and execute the script using

```
PYTHONPATH=<directory containing find_minimal_pairs> python3 Script_Analysis_BruteForce_Dimensionality.py
```
"""


import time
import numpy as np
from matplotlib import pyplot as plt
from find_minimal_pairs import (BestPairsFinder,
                                BruteForceFinder,
                                FastPairingFinder,
                                ParticleGenerator)


# Time the execution of a function
def time_solution(finder: BestPairsFinder) -> float:
    start = time.perf_counter()
    finder.solve()
    end = time.perf_counter()

    return end - start


points_limits = (2, 21) # upper limit exclusive


# Setup variables
n_iter = 100
dimensions = list(range(1, 5))
numbers_of_points = list(range(*points_limits, 2))


timings = np.zeros((len(dimensions), len(numbers_of_points)))


finder = BruteForceFinder(BestPairsFinder.distance,
                          [],
                          pre_sort=True,
                          starting_guess=FastPairingFinder)


# Benchmark...
for i, n_dim in enumerate(dimensions):
    for j, n_points in enumerate(numbers_of_points):
        print(f"Ndim = {n_dim:2d}, Npoints = {n_points:2d} ", end="", flush=True)

        t = np.zeros(n_iter)

        for k in range(n_iter):
            finder.points = ParticleGenerator.generate_particles(
                n_points, n_dim, seed=(i * len(numbers_of_points) * n_iter + j * n_iter + k)
            )

            t[k] = time_solution(finder)

            if n_iter < 10 or (k + 1) % (n_iter // 10) == 0:
                print(".", end="", flush=True)

        timings[i, j] = np.mean(t)
        print()


# ... and prepare plot
plt.rcParams.update({"text.usetex": True,
                     "font.family": "serif",
                     "figure.figsize": (6, 5)})

ax = plt.axes()
ax.set_xlim(points_limits)
ax.set_xticks(list(range(*points_limits, 4)))

for i, n_dim in enumerate(dimensions):
    plt.plot(numbers_of_points, timings[i, :], label=f"{n_dim:d}-D")

ax.spines["bottom"].set_linewidth("3")
ax.spines["top"].set_linewidth("3")
ax.spines["left"].set_linewidth("3")
ax.spines["right"].set_linewidth("3")
plt.tick_params(axis="x", length=6, width=3, labelsize=20, pad=10, direction="in")
plt.tick_params(axis="y", length=6, width=3, labelsize=20, pad=10, direction="in")
plt.legend(loc="best", fontsize=16, frameon=False)

ax.set_xlabel(r"$N_\mathrm{Points}$", fontsize=25)
ax.set_ylabel(r"$\left< t \right>$ / s", fontsize=25)


plt.tight_layout()
# plt.show()
plt.savefig("Analysis_BruteForce_Dimensionality.png", dpi=400)
plt.close()

