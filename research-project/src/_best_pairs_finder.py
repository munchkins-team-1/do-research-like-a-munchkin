import numpy as np
from numpy.core.overrides import set_module
from abc import ABCMeta, abstractmethod
from typing import Callable, List, Tuple
from inspect import signature
from .types import NDimPoint


@set_module("find_minimal_pairs")
class BestPairsFinder(metaclass=ABCMeta):
    """Abstract base class to find best pairs of points to
    minimize a function `f`.
    """

    def __init__(self,
                 f: Callable[[NDimPoint, NDimPoint], float],
                 points: List[NDimPoint]) -> None:
        """Constructor:

        Args:
            f: Function that returns float for two points. Arguments may be
                single floats, lists of floats, or one-dimensional NumPy arrays.
            points: A list of n-dimensional points to be grouped into pairs.

        Raises:
            TypeError: If `f` is not a function.
            ValueError: If `f` does not take exactly two arguments.
        """
        if not callable(f):
            raise TypeError("f is not callable!")

        if len(signature(f).parameters) != 2:
            raise ValueError("f must take exactly two arguments!")

        self.f = f

        self._points = []
        self.pair_mat = np.array([])
        self.n_points = 0

        self.points = points

    @property
    def points(self):
        """List of NDimPoints"""
        return self._points

    @points.setter
    def points(self, l_points: List[NDimPoint]):
        """
        Args:
            l_points: List of n-dimensional points

        Raises:
            ValueError: If number of points is odd.
        """
        if len(l_points) % 2 != 0:
            raise ValueError("The number of points is not even.")

        self._points = l_points
        self.n_points = len(l_points)
        self.pair_mat = self.func_matrix()

    @points.deleter
    def points(self):
        del self._points

    @staticmethod
    def distance(point1: NDimPoint, point2: NDimPoint) -> float:
        """Calculates the euclidian distance between two points in
        n-dimensional space.

        Args:
            point1: float, 1D list, or 1D NumPy array
            point2: float, 1D list, or 1D NumPy array

        Returns:
            Distance between `point1` and `point2`.

        Raises:
            ValueError: If points are not 1 column vectors.
            ValueError: If points do not have the same dimensionality.

        Examples:
            Distance between 1D-points, represented as floats:
            >>> BestPairsFinder.distance(2, 3)
            1

            Distance between 2D-points:
            >>> BestPairsFinder.distance((0, 0), (1, 1))
            1.4142135623730951
        """
        p1 = np.asarray(point1)
        p2 = np.asarray(point2)

        if len(p1.shape) > 1:
            raise ValueError("Point1 is not a vector!")

        if len(p2.shape) > 1:
            raise ValueError("Point2 is not a vector!")

        if p1.shape != p2.shape:
            raise ValueError("The points do not have the same dimensionality!")

        return np.linalg.norm(p1 - p2)

    def func_matrix(self) -> np.ndarray:
        """ Calculates a matrix of the function `f` for all pairs in points.

        Returns:
            Matrix that is filled with values of f for pairs.

        Examples:
            Distance matrix for two 1D points (note that `BestPairsFinder` cannot
            be instantiated directly):
            >>> finder = FastPairingFinder(BestPairsFinder.distance, [2, 3])
            >>> finder.func_matrix()
            array([[0., 1.],
                   [1., 0.]])
        """
        dist_mat = np.zeros(shape=(self.n_points, self.n_points), dtype=np.float64)

        for i in range(self.n_points):
            for j in range(i + 1, self.n_points):
                dist_mat[i, j] = self.f(self._points[i], self._points[j])
                dist_mat[j, i] = dist_mat[i, j]
        return dist_mat

    def evaluate_pairing(self, pairing: List[Tuple[int, int]]) -> float:
        """Calculates the target function for a given pairing of points."""
        return sum(self.pair_mat[pair] for pair in pairing)

    @abstractmethod
    def solve(self) -> Tuple[List[Tuple[int, int]], float]:
        pass

