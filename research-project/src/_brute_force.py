import numpy as np
from numpy.core.overrides import set_module
from typing import Callable, List, Optional, Tuple, Type
from .types import NDimPoint
from ._best_pairs_finder import BestPairsFinder
from ._fast_pairing import FastPairingFinder


@set_module("find_minimal_pairs")
class BruteForceFinder(BestPairsFinder):
    """Group points into pairs to minimize a target function `f` using
    the [Brute Force Method](https://munchkins-team-1.gitlab.io/do-research-like-a-munchkin/#the-brute-force-method).
    """

    def __init__(self,
                 f: Callable[[NDimPoint, NDimPoint], float],
                 points: List[NDimPoint],
                 pre_sort: bool = True,
                 starting_guess: Optional[Type[BestPairsFinder]] = FastPairingFinder,
                 **kwargs) -> None:
        """Constructor:

        Args:
            f: Function that returns float for two points. Arguments may be
                single floats, lists of floats, or one-dimensional NumPy arrays.
            points: A list of n-dimensional points to be grouped into pairs.
            pre_sort: Whether or not to sort the particles based on the sum of
                their distance to all other particles. Allows for fewer
                combinations to be checked.
            starting_guess: A type of `BestPairsFinder` to
                use as the initial guess for the brute-force calculation. If
                `None`, no initial guess is used.
            **kwargs: Keyword arguments for the constructor of the `starting_guess`
                finder.
        """
        if starting_guess is not None:
            self.starting_guess = starting_guess(f, [], **kwargs)
        else:
            self.starting_guess = None
        self.pre_sort = pre_sort
        super().__init__(f, points)

    @property
    def points(self) -> List[NDimPoint]:
        return super().points

    @points.setter
    def points(self, l_points: List[NDimPoint]) -> None:
        BestPairsFinder.points.fset(self, l_points)

        if self.starting_guess is not None:
            self.starting_guess._points = self._points
            self.starting_guess.n_points = self.n_points
            self.starting_guess.pair_mat = self.pair_mat

    @points.deleter
    def points(self):
        del super().points
        del self.starting_guess.points

    def solve(self) -> Tuple[List[Tuple[int, int]], float]:
        r"""Returns a pairing of points that minimizes the sum of f(p1, p2),
        as well as the corresponding sum.

        Note:
            While this version is guaranteed to provide the exact minimum, its
            \(\mathcal{O}\left( \frac{n!}{(n/2)! \cdot 2^{n/2}} \right)\) scaling
            behavior makes it take very long very quickly. Running with more
            than 20 particles at your own risk!

        Returns:
            Tuple containing the optimal pairing, and the corresponding target
            function.

        Examples:
            Trivial example for two points in 1D:
            >>> finder = BruteForceFinder(BestPairsFinder.distance, [2, 3])
            >>> finder.solve()
            ([(0, 1)], 1.0)

            Only one solution leading to the minimal sum will be returned:
            >>> finder = BruteForceFinder(BestPairsFinder.distance, [(1, 1), (-1, 1), (-1, -1), (1, -1)])
            >>> finder.solve()
            ([(0, 1), (2, 3)], 4.0)
        """
        (pairing, *_), target_function = self.solve_all()
        return pairing, target_function

    def solve_all(self) -> Tuple[List[List[Tuple[int, int]]], float]:
        r"""Returns a list of all possible pairings of points that minimize the
        sum of f(p1, p2), as well as the corresponding sum.

        Note:
            While this version is guaranteed to provide the exact minimum, its
            \(\mathcal{O}\left( \frac{n!}{(n/2)! \cdot 2^{n/2}} \right)\) scaling
            behavior makes it take very long very quickly. Running with more
            than 20 particles at your own risk!

        Returns:
            Tuple containing a list of optimal pairings, and the
            corresponding target function.

        Examples:
            Trivial example for two points in 1D:
            >>> finder = BruteForceFinder(BestPairsFinder.distance, [2, 3])
            >>> finder.solve_all()
            ([[(0, 1)]], 1.0)

            All solutions leading to the minimal sum will be returned:
            >>> finder = BruteForceFinder(BestPairsFinder.distance, [(1, 1), (-1, 1), (-1, -1), (1, -1)])
            >>> finder.solve_all()
            ([[(0, 1), (2, 3)], [(0, 3), (1, 2)]], 4.0)
        """
        if self.n_points == 0:
            return [[]], 0.0

        if self.starting_guess is not None:
            lowest_pairing, lowest_guess = self.starting_guess.solve()
        else:
            lowest_guess = np.inf

        # Sort the particles by the sum of their distances to all other
        # particles, starting with the smallest. This way, the helper
        # function will abort quicker.
        if self.pre_sort:
            indices = np.argsort(self.pair_mat.sum(axis=1))[::-1].tolist()
        else:
            indices = list(range(self.n_points))
        pairings, target_function = self._recursion_helper(
            remaining_indices=indices,
            current_pairing=[],
            current_sum=0.0,
            current_best_pairing=[],
            current_min_sum=lowest_guess
        )

        # To catch rounding errors if the guess gave the exact minimum
        if not pairings and self.starting_guess is not None:
            return [lowest_pairing], lowest_guess
        return [sorted(pairing, key=lambda pair: pair[0]) for pairing in pairings], target_function

    def _recursion_helper(self,
                          remaining_indices: List[int],
                          current_pairing: List[Tuple[int, int]],
                          current_sum: float,
                          current_best_pairing: List[List[Tuple[int, int]]],
                          current_min_sum: float) -> Tuple[List[List[Tuple[int, int]]], float]:
        n_rem = len(remaining_indices)
        if n_rem == 0:
            if current_sum == current_min_sum:
                return current_best_pairing + [current_pairing], current_min_sum
            return [current_pairing], current_sum

        for i in range(1, n_rem):
            pair = (min(remaining_indices[0], remaining_indices[i]),
                    max(remaining_indices[0], remaining_indices[i]))

            if current_sum + self.pair_mat[pair] > current_min_sum:
                continue

            current_best_pairing, current_min_sum = self._recursion_helper(
                remaining_indices[1:i] + remaining_indices[i + 1:],
                current_pairing + [pair],
                current_sum + self.pair_mat[pair],
                current_best_pairing,
                current_min_sum
            )

        return current_best_pairing, current_min_sum

