import numpy as np
from numpy.core.overrides import set_module
from typing import List, Tuple
from ._best_pairs_finder import BestPairsFinder


@set_module("find_minimal_pairs")
class FastPairingFinder(BestPairsFinder):
    """Group points into pairs to minimize a target function `f` using
    the [Fast Pairing Method](https://munchkins-team-1.gitlab.io/do-research-like-a-munchkin/#fast-pairing-method).
    """

    def solve(self) -> Tuple[List[Tuple[int, int]], float]:
        """Returns a list of pairs of points that minimizes the sum of f(p1, p2).

        Note:
            This algorithm is only guaranteed to provide a true minimum
            for 1D points; for higher dimensions, the result will generally
            not be minimal, but close to the true minimum. Also, only one
            solution is returned, even if there are multiple possible
            pairings to achieve it.

        Returns:
            Tuple containing the list of optimal pairs, and the corresponding target function.

        Examples:
            Trivial example for two 1D points:
            >>> finder = FastPairingFinder(BestPairsFinder.distance, [2, 3])
            >>> finder.solve()
            ([(0, 1)], 1.0)

            When there are multiple solutions, only one of them is returned:
            >>> finder = FastPairingFinder(BestPairsFinder.distance, [(1, 1), (-1, 1), (-1, -1), (1, -1)])
            >>> finder.solve()
            ([(0, 1), (2, 3)], 4.0)
        """
        if self.n_points == 0:
            return [], 0.0

        list_of_pairs = []
        mask = np.ma.asarray(self.pair_mat)

        line_sums = np.sum(mask, axis=1)

        for i in range(self.n_points):
            mask[i, i] = np.ma.masked

        while mask.all() is not np.ma.masked:
            # Find the row with the highest sum (which has not been masked)
            ind_max_line = line_sums.argmax(fill_value=0)

            # Find the smallest element within that row
            ind_min_elem = mask[ind_max_line].argmin(fill_value=np.inf)

            list_of_pairs.append((min(ind_max_line, ind_min_elem), max(ind_max_line, ind_min_elem)))

            # Delete the corresponding rows and columns...
            mask[ind_max_line, :] = np.ma.masked
            mask[ind_min_elem, :] = np.ma.masked
            mask[:, ind_min_elem] = np.ma.masked
            mask[:, ind_max_line] = np.ma.masked

            # ... and delete the row from the sums to be checked
            # TODO: Should we re-compute the sums here?
            line_sums[ind_max_line] = np.ma.masked
            line_sums[ind_min_elem] = np.ma.masked

        # Sort the list by the pairs' first elements
        return sorted(list_of_pairs, key=lambda pair: pair[0]), self.evaluate_pairing(list_of_pairs)

