import numpy as np
from typing import List, Tuple, Union


# NDimPoint = np.typing.ArrayLike # For when numpy 1.21 releases
NDimPoint = Union[float, List[float], Tuple[float, ...], np.ndarray]
"""An n-dimensional point which can be converted to a NumPy array of shape `(n,)`"""


SeedType = Union[int, List[int], Tuple[int, ...], np.ndarray,
                 np.random.SeedSequence, np.random.BitGenerator,
                 np.random.Generator]
"""Types that can be used for a NumPy random number generator."""

