import numpy as np
from numpy.core.overrides import set_module
from copy import deepcopy
from typing import Callable, List, Optional, Tuple, Type
from .types import NDimPoint
from ._best_pairs_finder import BestPairsFinder
from ._fast_pairing import FastPairingFinder


@set_module("find_minimal_pairs")
class MonteCarloFinder(BestPairsFinder):
    """Group points into pairs to minimize a target function `f` using
    the [Monte Carlo Method](https://munchkins-team-1.gitlab.io/do-research-like-a-munchkin/#monte-carlo-approach).
    """

    def __init__(self,
                 f: Callable[[NDimPoint, NDimPoint], float],
                 points: List[NDimPoint],
                 n_moves: int,
                 temperature: float = 1.0,
                 starting_guess: Type[BestPairsFinder] = FastPairingFinder,
                 **kwargs) -> None:
        """Constructor:

        Args:
            f: Function that returns float for two points. Arguments may be
                single floats, lists of floats, or one-dimensional NumPy arrays.
            points: A list of n-dimensional points to be grouped into pairs.
            n_moves: Number of MC moves to be performed.
            temperature: "Temperature" of the sampling.
            starting_guess: A type of `BestPairsFinder` to
                use as the initial guess for the MC sampling.
            **kwargs: Keyword arguments for the constructor of the `starting_guess`
                finder.

        Raises:
            ValueError: If `n_moves` is less than 1.
            ValueError: If `temperature` is less than 0.
        """
        self.starting_guess = starting_guess(f, [], **kwargs)

        super().__init__(f, points)

        if n_moves < 1:
            raise ValueError("Given number of moves is too small.")
        if temperature < 0.0:
            raise ValueError("Temperature is below 0. It will break the Metropolis criterion.")

        self.n_moves = n_moves
        self.temperature = temperature

    @property
    def points(self) -> List[NDimPoint]:
        return super().points

    @points.setter
    def points(self, l_points: List[NDimPoint]) -> None:
        BestPairsFinder.points.fset(self, l_points)

        self.starting_guess._points = self._points
        self.starting_guess.n_points = self.n_points
        self.starting_guess.pair_mat = self.pair_mat

    @points.deleter
    def points(self):
        del super().points
        del self.starting_guess.points

    def solve(self) -> Tuple[List[Tuple[int, int]], float]:
        """Returns a list of pairs of points that minimizes the sum of f(p1, p2).

        Returns:
            Tuple containing the list of optimal pairs, and the corresponding target function.

        Examples:
            Trivial example in 1D:
            >>> finder = MonteCarloFinder(BestPairsFinder.distance, [2, 3], 1000)
            >>> finder.solve()
            ([(0, 1)], 1.0)
        """
        if self.n_points == 0:
            return [], 0.0

        curr_pairing, curr_estimate = self.starting_guess.solve()

        lowest_pairing = deepcopy(curr_pairing)
        lowest_estimate = curr_estimate

        for _ in range(self.n_moves):
            first, second = self.swap_entries(curr_pairing)
            new_estimate = self.evaluate_pairing(curr_pairing)

            if self.metropolis(prev=curr_estimate, new=new_estimate):
                curr_estimate = new_estimate

                if curr_estimate < lowest_estimate:
                    lowest_pairing[:] = curr_pairing[:]
                    lowest_estimate = curr_estimate
            else:
                self.swap_entries(curr_pairing, first, second)

        return sorted(lowest_pairing, key=lambda pair: pair[0]), lowest_estimate

    def metropolis(self, prev: float, new: float) -> bool:
        """Evaluates whether or not a new guess will be accepted according to
        the Metropolis criterion.
        """
        return new < prev or np.exp(-(new - prev) / self.temperature) > np.random.random()

    @staticmethod
    def swap_entries(pairing: List[Tuple[int, int]],
                     first: Optional[Tuple[int, int]] = None,
                     second: Optional[Tuple[int, int]] = None) -> Tuple[Tuple[int, int], Tuple[int, int]]:
        """Swaps two entries in the input list. The indices of
        the swapped entries are returned in case the swap needs to be
        undone if the new guess is not accepted.

        Args:
            pairing: List of tuples of indices for the current pairing.
            first: Tuple containing the index of the first pair and the
                index of the point inside the pair. By default `None`,
                in which case it is randomly generated.
            second: Tuple containing the index of the second pair and the
                index of the point inside the pair. By default `None`,
                in which case it is randomly generated.

        Returns:
            The indices of the swapped entries as tuples.

        Raises:
            IndexError: If the pair or point indices are invalid.
        """
        n_pairs = len(pairing)

        if first is not None and second is not None:
            pair_1, point_1 = first
            pair_2, point_2 = second

            if (pair_1 < 0
                or pair_1 >= n_pairs
                or pair_2 < 0
                or pair_2 >= n_pairs
                ):
                raise IndexError("Pair indices must be between 0 and n_pairs - 1.")
            if (point_1 < 0
                or point_1 >= 2
                or point_2 < 0
                or point_2 >= 2
                ):
                raise IndexError("Point indices must be 0 or 1.")
        else:
            pair_1 = np.random.randint(n_pairs)
            while (pair_2 := np.random.randint(n_pairs)) == pair_1:
                pass

            point_1 = np.random.randint(2)
            point_2 = np.random.randint(2)

        # Conserve the ordering of the tuples
        tmp_1 = [0, 0]
        tmp_1[point_1] = pairing[pair_2][point_2]
        tmp_1[point_1 ^ 1] = pairing[pair_1][point_1 ^ 1]

        tmp_2 = [0, 0]
        tmp_2[point_2] = pairing[pair_1][point_1]
        tmp_2[point_2 ^ 1] = pairing[pair_2][point_2 ^ 1]

        pairing[pair_1] = tuple(tmp_1)
        pairing[pair_2] = tuple(tmp_2)

        return (pair_1, point_1), (pair_2, point_2)

