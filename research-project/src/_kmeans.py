import numpy as np
from numpy.core.overrides import set_module
from sklearn.cluster import KMeans
from typing import Callable, List, Optional, Tuple
from .types import NDimPoint
from ._best_pairs_finder import BestPairsFinder
from ._fast_pairing import FastPairingFinder
from ._brute_force import BruteForceFinder


@set_module("find_minimal_pairs")
class KMeansFinder(BestPairsFinder):
    """Group points into pairs to minimize a target function `f` using
    the [K-Means Clustering Method](https://munchkins-team-1.gitlab.io/do-research-like-a-munchkin/#k-means-clustering).
    """

    def __init__(self,
                 f: Callable[[NDimPoint, NDimPoint], float],
                 points: List[NDimPoint],
                 n_clusters: int) -> None:
        """Constructor:

        Args:
            f: Function that returns float for two points. Arguments may be
                single floats, lists of floats, or one-dimensional NumPy arrays.
            points: A list of n-dimensional points to be grouped into pairs.
            n_clusters: Lower bound for the number of even-sized clusters to be
                formed.

        Raises:
            ValueError: If `n_clusters` is less than 1.
        """
        super().__init__(f, points)

        if n_clusters < 1:
            raise ValueError("`n_clusters` must be at least 1!")
        self.n_clusters = n_clusters

    def solve(self) -> Tuple[List[Tuple[int, int]], float]:
        """Form even-sized clusters using the K-Means algorithm as used in
        `KMeansFinder.get_even_sized_clusters`, and find the ideal pairing
        in each cluster using the brute-force method, with the FastPairing method
        as a starting guess.

        Returns:
            Tuple containing a list of optimal pairings, and the
            corresponding target function.
        """
        if self.n_points == 0:
            return [], 0.0

        clusters = self.get_even_sized_clusters()
        cluster_solver = BruteForceFinder(self.f, [])

        # Create a key to map each point in the clusters to
        # its actual index in the points array, so key[i][j] = k
        # means the j-th point in the i-th cluster is the k-th point
        # in self._points
        key = [[] for _ in range(len(clusters))]
        for i, cluster in enumerate(clusters):
            for cluster_point in cluster:
                for j, point in enumerate(self._points):
                    if np.allclose(point, cluster_point):
                        key[i] += [j]
                        break

        pairs = []
        s = 0.0

        for i, cluster in enumerate(clusters):
            # For now, let's be satisfied with the first solution
            cluster_solver.points = cluster
            cluster_pairs, cluster_sum = cluster_solver.solve()

            pairs += [(key[i][j], key[i][k]) for (j, k) in cluster_pairs]
            s += cluster_sum

        return sorted(pairs, key=lambda pair: pair[0]), s

    def get_even_sized_clusters(self) -> List[List[NDimPoint]]:
        """Cluster `types.NDimPoint`s based on their euclidean distance with
        the KMeans algorithm into clusters of even size.

        Note:
            The number of clusters may be altered to handle clusters
            of uneven size.

        Returns:
            List of even sized clusters of points.

        Examples:
            Trivial example in 1D:
            >>> finder = KMeansFinder(BestPairsFinder.distance, [1, 2, 3, 4, 5, 6], 2)
            >>> finder.get_even_sized_clusters()
            [[[4], [3]], [[5], [6]], [[1], [2]]]

            Note that even though we specified two clusters, the algorithm
            produced three clusters of size 2 (otherwise, we would have
            gotten two clusters of size 3).
        """
        # get index of cluster for each point and fitted KMeans model
        cluster, km = self.kmeans(self.n_clusters)

        if cluster is None and km is None:
            return []

        # store points sorted by cluster
        uneven_clusters = []
        centers_uneven_cluster = []
        even_clusters = []

        cluster = np.array(cluster)
        # To handle 1D points...
        if np.isscalar(self._points[0]):
            points = np.array(self._points).reshape(-1, 1)
        else:
            points = np.array(self._points)

        for i in range(self.n_clusters):
            mask = cluster == i
            if len(points[mask]) % 2 == 0:
                even_clusters += [points[mask]]
            else:
                uneven_clusters += [points[mask]]
                centers_uneven_cluster += [km.cluster_centers_[i]]

        # handle clusters of uneven size
        if uneven_clusters:
            # pair cluster centers
            # TODO: We should be able to use any metric here...
            best_center_pairs = FastPairingFinder(BestPairsFinder.distance, centers_uneven_cluster)

            # TODO: Should we also use the recursive or MC pairing here?
            paired_clusters, _ = best_center_pairs.solve()

            # get clusters with even number of elements
            for c in paired_clusters:
                # get distance of all points of paired cluster
                # TODO: Same as above, when we implement different metrics for
                # K-Means, np.linalg.norm should be replaced
                d0 = np.linalg.norm(uneven_clusters[c[0]] - centers_uneven_cluster[c[1]], axis=1)
                d1 = np.linalg.norm(uneven_clusters[c[1]] - centers_uneven_cluster[c[0]], axis=1)

                # get index of minimal elements
                ind = np.argmin(d0), np.argmin(d1)

                # add closest pair of points and remaining cluster to even clusters
                even_clusters += [np.array([uneven_clusters[c[0]][ind[0]], uneven_clusters[c[1]][ind[1]]])]

                # remove the points from the uneven cluster and check if any points remain
                for i in range(2):
                    new_even = np.array([*uneven_clusters[c[i]][:ind[i]], *uneven_clusters[c[i]][ind[i] + 1:]])
                    if new_even.size > 0:
                        even_clusters += [new_even]

        # convert list of np.ndarrays to list of lists
        for i, cluster in enumerate(even_clusters):
            even_clusters[i] = cluster.tolist()

        return even_clusters

    def kmeans(self,
               n_init: int = 10,
               max_iter: int = 300,
               seed: int = 42) -> Tuple[Optional[np.ndarray], Optional[KMeans]]:
        """k-means algorithm to cluster points based on their euclidian distance using the sum-of-squares criterion

        This algorithm uses the sklearn python library
        See the documentation here: https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html

        Args:
            n_init: Number of time the k-means algorithm will be run with different centroid seeds.
                The final results will be the best output of `n_init` consecutive runs in terms of inertia.
            max_iter: Maximum number of iterations of the k-means algorithm for a single run.
            seed: Determines random number generation for centroid initialization.

        Returns:
            List of indices of the cluster each sample belongs to and fitted KMeans model.

        Examples:
            Trivial example in 1D:
            >>> finder = KMeansFinder(BestPairsFinder.distance, [1, 2, 3, 4, 5, 6], 2)
            >>> clusters, _ = finder.kmeans()
            >>> clusters
            [1, 1, 1, 0, 0, 0]
        """
        if self.n_points == 0:
            return None, None

        # prepare points for application of sklearn.KMeans
        if np.isscalar(self._points[0]):
            points = np.array(self._points).reshape(-1, 1)
        else:
            points = np.array(self._points)

        # init KMeans
        km = KMeans(init="k-means++",
                    n_clusters=self.n_clusters,
                    n_init=n_init,
                    max_iter=max_iter,
                    random_state=seed)

        # fit km model and predict clusters
        cluster = km.fit_predict(points)

        return cluster, km

