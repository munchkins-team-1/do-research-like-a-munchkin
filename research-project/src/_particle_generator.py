import numpy as np
from numpy.core.overrides import set_module
from typing import List, Optional
from .types import SeedType


@set_module("find_minimal_pairs")
class ParticleGenerator:
    @staticmethod
    def generate_particles(n_particles: int,
                           n_dimensions: int,
                           min_val: float = 0.0,
                           max_val: float = 1.0,
                           seed: Optional[SeedType] = None
                           ) -> List[np.ndarray]:
        """Creates n-dimensional particles with randomized coordinates,
        represented as a list of NumPy arrays. All coordinates will be
        within the half-open interval [`min_val`, `max_val`).

        Args:
            n_particles: Number of particles to create.
            n_dimensions: Number of dimensions for the particle coordinates.
            min_val: Minimum value for particle coordinates (inclusive).
            max_val: Maximum value for particle coordinates (exclusive).
            seed: Seed for random number generator.

        Returns:
            List of length `n_particles` containing `n_dimensions`-dimensional arrays.

        Raises:
            ValueError: If `n_particles` is zero or negative.
            ValueError: If `n_dimensions` is zero or negative.
            ValueError: If `min_val` is greater than or equal to `max_val`.
        """
        if n_particles <= 0:
            raise ValueError("Number of particles must be at least 1")
        if n_dimensions <= 0:
            raise ValueError("Number of dimensions must be at least 1")
        if min_val >= max_val:
            raise ValueError("`min_val` must be less than `max_val`")

        rng = np.random.default_rng(seed)
        return [(max_val - min_val) * rng.random((n_dimensions,)) + min_val for _ in range(n_particles)]

