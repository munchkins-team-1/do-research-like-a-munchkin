import pytest
import numpy as np
from copy import deepcopy
from .. import (BestPairsFinder,
                FastPairingFinder,
                BruteForceFinder,
                MonteCarloFinder,
                KMeansFinder,
                ParticleGenerator)


def test_init_not_callable():
    with pytest.raises(TypeError):
        FastPairingFinder("I'm not a function!", [])


def test_init_wrong_number_of_arguments():
    with pytest.raises(ValueError):
        FastPairingFinder(lambda x, y, z: x + y + z, [])


@pytest.mark.parametrize("point1, point2, expected_dist",
                         [(1, 2, 1),
                          (1, -1, 2),
                          ([0, 0, 0], [1, 1, 1], np.sqrt(3)),
                          ]
                         )
def test_distance(point1, point2, expected_dist):
    assert BestPairsFinder.distance(point1, point2) == expected_dist


def test_not_vector_distance():
    with pytest.raises(ValueError, match=r"Point[12] is not a vector!"):
        BestPairsFinder.distance([[0, 0, 0], [1, 1, 1]], [0, 0, 0])


def test_not_same_shape_distance():
    with pytest.raises(ValueError, match=r".* dimensionality.*"):
        BestPairsFinder.distance([0, 0, 0], [0, 1])


def test_uneven_points():
    with pytest.raises(ValueError):
        pairs_finder = FastPairingFinder(BestPairsFinder.distance, [1, 2, 3])


def test_func_matrix():
    pairs_finder = FastPairingFinder(BestPairsFinder.distance, [-10, -1, -2, 1])
    np.testing.assert_allclose(pairs_finder.func_matrix(), np.array([[0, 9, 8, 11],
                                                                     [9, 0, 1, 2],
                                                                     [8, 1, 0, 3],
                                                                     [11, 2, 3, 0]]))


def test_find_pairs_fast_pairing():
    pairs_finder = FastPairingFinder(BestPairsFinder.distance, [-10, -1, -2, 1])
    pairs, result = pairs_finder.solve()
    np.testing.assert_array_equal(pairs, [(0, 2), (1, 3)])
    assert result == 10


def test_swap_entries_monte_carlo():
    pairing = [(0, 1), (2, 3), (4, 5), (6, 7), (8, 9)]
    MonteCarloFinder.swap_entries(pairing)
    for i, (first, second) in enumerate(pairing):
        assert all(first not in other_pair for other_pair in pairing[:i] + pairing[i + 1:])
        assert all(second not in other_pair for other_pair in pairing[:i] + pairing[i + 1:])


def test_revert_swap():
    pairing = [(0, 1), (2, 3), (4, 5), (6, 7), (8, 9)]
    old_pairing = deepcopy(pairing)
    first, second = MonteCarloFinder.swap_entries(pairing)
    MonteCarloFinder.swap_entries(pairing, first, second)
    for (p1, p2) in pairing:
        assert (p1, p2) in old_pairing or (p2, p1) in old_pairing


def test_metropolis():
    pairs_finder = MonteCarloFinder(BestPairsFinder.distance, [], 1)
    assert pairs_finder.metropolis(prev=1.0, new=0.5)
    assert not pairs_finder.metropolis(prev=0.0, new=np.inf)


def test_empty_input():
    pairs_finder = BruteForceFinder(BestPairsFinder.distance, [])
    assert pairs_finder.solve() == ([], 0)
    assert pairs_finder.solve_all() == ([[]], 0)


def test_number_of_pairs():
    # Random even number between 2 and 10
    n_points = 2 * np.random.randint(1, 6)
    n_dim = np.random.randint(1, 4)

    points = ParticleGenerator.generate_particles(n_points, n_dim)
    pairs_finder = FastPairingFinder(BestPairsFinder.distance, points)
    pairs, _ = pairs_finder.solve()
    assert len(pairs) == n_points // 2


def test_two_elements_per_pair():
    # Random even number between 2 and 10
    n_points = 2 * np.random.randint(1, 6)
    n_dim = np.random.randint(1, 4)

    points = ParticleGenerator.generate_particles(n_points, n_dim)
    pairs_finder = FastPairingFinder(BestPairsFinder.distance, points)
    pairs, _ = pairs_finder.solve()
    assert all(len(pair) == 2 for pair in pairs)


def test_no_duplicates():
    # Random even number between 2 and 10
    n_points = 2 * np.random.randint(1, 6)
    n_dim = np.random.randint(1, 4)

    points = ParticleGenerator.generate_particles(n_points, n_dim)
    pairs_finder = FastPairingFinder(BestPairsFinder.distance, points)
    pairs, _ = pairs_finder.solve()
    for i, (first, second) in enumerate(pairs):
        assert all(first not in other_pair for other_pair in pairs[i + 1:])
        assert all(second not in other_pair for other_pair in pairs[i + 1:])


def test_two_solutions_for_square():
    points = [(1, 1), (-1, 1), (-1, -1), (1, -1)]
    pairs_finder = BruteForceFinder(BestPairsFinder.distance, points)
    pairs, _ = pairs_finder.solve_all()

    assert len(pairs) == 2
    assert [(0, 1), (2, 3)] in pairs
    assert [(0, 3), (1, 2)] in pairs


@pytest.mark.parametrize("points, expected_clusters",
                         [([1, 2, 3, 4], [1, 1, 0, 0]),
                          ([[1, 2], [3, 4], [5, 6], [7, 8]], [1, 1, 0, 0]),
                          ([1, 4, 2, 1, 5, 6], [0, 1, 0, 0, 1, 1])
                          ]
                         )
def test_kmeans(points, expected_clusters):
    pairs_finder = KMeansFinder(BestPairsFinder.distance, points, 2)
    cluster, _ = pairs_finder.kmeans()
    assert np.all(cluster == expected_clusters)


@pytest.mark.parametrize("points, n_clusters",
                         [(np.arange(0, 20, 1), 4),
                          ([[1, 2], [3, 4], [5, 6], [7, 9], [9, 10], [11, 12]], 2)
                          ])
def test_even_sized_clusters_kmean(points, n_clusters):
    pairs_finder = KMeansFinder(BestPairsFinder.distance, points, n_clusters)
    clusters = pairs_finder.get_even_sized_clusters()
    assert all(len(cluster) % 2 == 0 for cluster in clusters)


def test_brute_force_set_points_with_guess():
    pairs_finder = BruteForceFinder(BestPairsFinder.distance, [], starting_guess=FastPairingFinder)
    points = [1, 2, 3, 4, 5, 6]

    pairs_finder.points = points

    assert np.array_equal(points, pairs_finder._points)
    assert np.array_equal(points, pairs_finder.starting_guess._points)

    assert pairs_finder.n_points == 6
    assert pairs_finder.starting_guess.n_points == 6

