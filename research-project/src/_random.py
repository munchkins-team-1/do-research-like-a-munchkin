import numpy as np
from numpy.core.overrides import set_module
from typing import Callable, List, Tuple
from .types import NDimPoint, SeedType
from ._best_pairs_finder import BestPairsFinder


@set_module("find_minimal_pairs")
class RandomPairFinder(BestPairsFinder):
    """Group points into pairs at random."""

    def __init__(self,
                 f: Callable[[NDimPoint, NDimPoint], float],
                 points: List[NDimPoint],
                 seed: SeedType = None) -> None:
        """Constructor:

        Args:
            f: Function that returns float for two points. Arguments may be
                single floats, lists of floats, or one-dimensional NumPy arrays.
            points: A list of n-dimensional points to be grouped into pairs.
            seed: Seed for random number generator.
        """
        super().__init__(f, points)
        self.seed = seed

    def solve(self) -> Tuple[List[Tuple[int, int]], float]:
        if self.n_points == 0:
            return [], 0.0

        rng = np.random.default_rng(self.seed)
        random_indices = rng.shuffle(np.arange(self.n_points))
        pairs = [(random_indices[i], random_indices[i + 1]) for i in range(0, self.n_points, 2)]

        return pairs, self.evaluate_pairing(pairs)

