# Do Research Like a Munchkin - Team 1

Shared repository for the [Do Research Like a Munchkin](https://the-munchkins.gitlab.io/do-research-like-a-munchkin)
programming course.

For a detailed overview of our work and findings on the research project,
visit https://munchkins-team-1.gitlab.io/do-research-like-a-munchkin.

## Requirements

To run the code and generate the documentation for the research project,
you will need the following:

* [Python](https://www.python.org) 3.6 or newer
* [NumPy](https://numpy.org)
* [matplotlib](https://matplotlib.org)
* [scikit-learn](https://scikit-learn.org)
* [MkDocs](https://www.mkdocs.org)
* [pdoc3](https://pdoc3.github.io/pdoc/)

## How to compile the documentation

To build the *user documentation* for the research project, simply navigate
to the root of the repository and run
```
mkdocs build
```
This will generate the HTML documentation in the `site` directory.

To get the *API documentation*, you first need to "build" the source code,
which means simply copying the `src` directory to its module destination.
For this, navigate to `research-project` and run
```
cp -r src find_minimal_pairs
```
Then, navigate back to the project root and build the API documentation with
[pdoc3](https://pdoc3.github.io/pdoc/):
```
pdoc3 --html -o site/code_docs -c latex_math=True research-project/find_minimal_pairs
```
This will build the API documentation inside `site/code_docs/find_minimal_pairs`.
For proper references from the user documentation, the contents need to be
moved to the `site/code_docs` directory. To do so, simply run
```
mv site/code_docs/find_minimal_pairs/* site/code_docs/
rm -r site/code_docs/find_minimal_pairs
```
To inspect the documentation, you can navigate to the `site` directory
and run
```
python -m http.server
```
This will host the contents of the directory on `localhost`, which makes
the documentation accessible from your browser. Note that you sadly
*cannot* host both the user and API documentation using `mkdocs serve`,
as that command overwrites the `site` directory and therefore deletes
the API documentation in the process.

