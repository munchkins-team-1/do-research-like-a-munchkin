#!/usr/bin/env python3
import sys
if sys.version_info >= (3, 9):
    from collections.abc import Iterable
else:
    from typing import Iterable


def sum_of_multiples(maxval: int, factors: Iterable[int]) -> int:
    """ Calculates the sum of all multiples of `factors` less
    than `maxval`.

    Args:
        maxval: Maximum value.
        factors: Iterable of factors.

    Returns:
        Sum of multiples of `factors` less than `maxval`.

    Raises:
        ValueError: If `maxval` is less than 1.
        ValueError: If any factor in `factors` is less than 1.

    Examples:
        ```
        >>> sum_of_multiples(1000, [3, 5])
        233168
        ```
    """
    if maxval < 1:
        raise ValueError("maxval must be greater than zero")

    if any(fac < 1 for fac in factors):
        raise ValueError("factors must all be positive and nonzero")

    return sum(i for i in range(1, maxval) if any(i % fac == 0 for fac in factors))


if __name__ == "__main__":
    print(sum_of_multiples(1000, [3, 5]))
